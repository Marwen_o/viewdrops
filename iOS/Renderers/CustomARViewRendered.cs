﻿using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using UIKit;
using ViewDrops;
using ViewDrops.iOS;

[assembly: ExportRenderer(typeof(CustomARView), typeof(CustomARViewRendered))]
namespace ViewDrops.iOS
{
	public class CustomARViewRendered : PageRenderer
	{
		public UIWindow Window { get; set; }
		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);
			ARViewController viewController = new ARViewController("", false, this);
			UINavigationController navController = new UINavigationController(viewController);
			Window = new UIWindow(UIScreen.MainScreen.Bounds);
			Window.RootViewController = navController;
			Window.MakeKeyAndVisible();
		}
	}
}
