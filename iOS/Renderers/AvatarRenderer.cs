﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using ViewDrops.iOS.Renderers;
using ViewDrops.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Avatar), typeof(AvatarRenderer))]
namespace ViewDrops.iOS.Renderers
{
	public class AvatarRenderer : ImageRenderer
	{
		private void CreateCircle()
		{
			try
			{
				double min = Math.Min(Element.Width, Element.Height);
				Control.Layer.CornerRadius = (float)(min / 2.0);
				Control.Layer.MasksToBounds = false;
				Control.Layer.BorderColor = ((Avatar)Element).BorderColor.ToCGColor();
				Control.Layer.BorderWidth = ((Avatar)Element).BorderThickness;
				Control.ClipsToBounds = true;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Unable to create circle image: " + ex);
			}
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);
			if (e.OldElement != null || Element == null)
				return;
			CreateCircle();
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (e.PropertyName == VisualElement.HeightProperty.PropertyName ||
				e.PropertyName == VisualElement.WidthProperty.PropertyName)
			{
				CreateCircle();
			}
		}
	}
}
