﻿using System;
using System.Collections.Generic;
using XLabs.Forms.Mvvm;

using Xamarin.Forms;
using ViewDrops.ViewModels.Drops;

namespace ViewDrops.Views.Drops.Template
{
	public partial class DropTemplate
	{
		public DropTemplate()
		{
			InitializeComponent();
			avatar.HeightRequest = avatar.WidthRequest = 50 * App.ScreenWidth / 376;
			avatar.BorderThickness = 5;
			avatar.BorderColor = Color.FromHex("#2AE0FC");
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			SetSizes();
		}

		void SetSizes()
		{
			var bc = BindingContext as ChallengeDrop;
			avatar.HeightRequest = avatar.WidthRequest = (bc.IsWinner ? 65 : 50) * App.ScreenWidth / 376;
			if (bc.IsWinner)
			{
				avatar.BorderColor = Color.FromHex("#2AE0FC");
			}
		}

	}
}
