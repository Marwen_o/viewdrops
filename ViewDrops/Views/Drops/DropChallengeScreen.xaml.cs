﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using ViewDrops.ViewModels.Drops;
using ViewDrops.Views.Drops.Template;

namespace ViewDrops
{
	public partial class DropChallengeScreen
	{
		public DropChallengeScreen()
		{
			InitializeComponent();
			BindingContext = new DropViewModel();
			NavigationPage.SetHasNavigationBar(this, false);
			SetSizes();
		}

		void SetSizes()
		{
			DropCircle.HeightRequest = DropCircle.WidthRequest = App.ScreenWidth;
			DropCircle.BorderColor = Color.FromHex("#2AE0FC");
			DropCircle.BorderThickness = 40;
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			if (ChallengeName.Text != null)
				ChallengeName.Text.ToUpper();

			var drops = (BindingContext as DropViewModel).Drops;
			ConstructDrops(drops);
		}

		void ConstructDrops(List<ChallengeDrop> drops)
		{
			if (drops == null || drops.Count == 0)
				return;
			foreach (var drop in drops)
			{
				StackOfDrops.Children.Add(MakeDropView(drop));
			}
		}

		DropTemplate MakeDropView(ChallengeDrop drop)
		{
			DropTemplate view = new DropTemplate();
			view.BindingContext = drop;
			return view;
		}
	}
}
