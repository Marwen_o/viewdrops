﻿using Xamarin.Forms;
using System;
using System.ComponentModel;

namespace ViewDrops.Views.Common
{
	public class Avatar : Image
	{
		public static readonly BindableProperty BorderThicknessProperty =
			BindableProperty.Create("", typeof(int), typeof(RoundedBox), 0);
		public int BorderThickness
		{
			get { return (int)GetValue(BorderThicknessProperty); }
			set
			{
				SetValue(BorderThicknessProperty, value);
			}
		}

		public static readonly BindableProperty BorderColorProperty =
			BindableProperty.Create("", typeof(Color), typeof(RoundedBox), Color.White);
		public Color BorderColor
		{
			get { return (Color)GetValue(BorderColorProperty); }
			set
			{
				SetValue(BorderColorProperty, value);
			}
		}
	}
}
