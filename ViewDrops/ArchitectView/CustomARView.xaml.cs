﻿using Xamarin.Forms;

namespace ViewDrops
{
	public partial class CustomARView : ContentPage
	{
		bool isURLInvocked;

		public bool IsURLInvocked
		{
			get
			{
				return isURLInvocked;
			}
			set
			{
				isURLInvocked = value;
				this.OnPropertyChanged("IsURLInvocked");
				if (IsURLInvocked)
				{
					System.Diagnostics.Debug.WriteLine("URL Is Invocked ! ");
					Navigation.PushModalAsync(new MainPage());
					System.Diagnostics.Debug.WriteLine("Main Page launched ! ");
					//PushChallengeView();
				}
			}
		}

		public CustomARView()
		{
			InitializeComponent();
		}

		async public void PushChallengeView()
		{
			System.Diagnostics.Debug.WriteLine("launch ! ");
			//var newPage = new MainPage();
			//newPage.Content = new DropChallengeScreen();
			//this.Content = new DropChallengeScreen();
			await Navigation.PushModalAsync(new MainPage());
		}
	}
}

