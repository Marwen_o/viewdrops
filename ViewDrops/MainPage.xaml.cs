﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ViewDrops
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
			Content = new DropChallengeScreen();
		}
	}
}
