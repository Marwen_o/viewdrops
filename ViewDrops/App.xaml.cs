﻿using Xamarin.Forms;

namespace ViewDrops
{
	public enum ViewType
	{
		ARView,
		Challenge
	}

	public partial class App : Application
	{
		public static double ScreenWidth;
		public static double ScreenHeight;
		public App()
		{
			InitializeComponent();

			MainPage = new ARView();
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
