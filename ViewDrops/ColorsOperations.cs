﻿
using Xamarin.Forms;
namespace ViewDrops
{
	
    /// <summary>
    /// Colors operations.
    /// </summary>
    public static class ColorsOperations
		{
			/// <summary>
			/// Tos the color of the hex.
			/// </summary>
			/// <returns>The hex color.</returns>
			/// <param name="color">Color.</param>
			public static string ToHexColor(this Color color)
			{
				int red = (int)(color.R * 255);
				int green = (int)(color.G * 255);
				int blue = (int)(color.B * 255);
				return string.Format("#{0}{1}{2}", red.ToString("X2"), green.ToString("X2"), blue.ToString("X2"));
			}
		}


}

