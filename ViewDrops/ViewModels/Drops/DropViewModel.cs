﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

using System.Collections.Generic;

namespace ViewDrops.ViewModels.Drops
{
	public class ChallengeDrop
	{
		public string SourceImage { get; set; }
		public string ChallengerName { get; set; }
		public bool IsWinner { get; set; }
	}

	public class DropViewModel 
	{
		public List<ChallengeDrop> Drops { get; set; }
		public string ChallengeName { private set; get; }

		public DropViewModel()
		{
			ChallengeName = "sick trick contest";
			Drops = new List<ChallengeDrop>
			{
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = true },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },
				new ChallengeDrop{ SourceImage = "messi.png", IsWinner = false },


			};
		}
	}
}
